from abc import ABC, abstractmethod
from dataclasses import dataclass
from typing import List, Dict, Union
from uuid import UUID

import redis


Item = Union[str, list, dict]
Transaction = str


class DB:
    def __init__(self, host: str = "localhost", port: int = 6379):
        self._conn = redis.Redis(host=host, port=port, db=0)
        return

    def get(self, uid: UUID) -> Dict[Item, Item]:
        return self._conn.hgetall(uid.hex)

    def set(self, uid: UUID, items: Dict[Item, Item]):
        self._conn.hset(uid.hex, mapping=items)


@dataclass
class DBObjectRef:
    uid: UUID


class DBObject(ABC):
    @staticmethod
    @abstractmethod
    def populate_from(db: DB, ref: DBObjectRef):
        raise NotImplementedError()

    @abstractmethod
    def write_to(self, db: DB, transaction: Transaction):
        raise NotImplementedError()


class Project(DBObject):
    def __init__(self, db: DB, uid: UUID, name: str, modules: List[UUID]):
        self.db = db
        self.uid = uid
        self.name = name
        self.modules = modules
        return

    @staticmethod
    def populate_from(db: DB, ref: DBObjectRef):
        fields = db.get(ref.uid)
        module_ids = fields["modules"]
        assert isinstance(module_ids, list)
        return Project(db, ref.uid, fields["name"], module_ids)

    def save(self, transaction: Transaction):
        self.db.set(self.uid, {
            "name": self.name,
            "modules": self.modules,
        })
        return


Address = int


class Section(DBObject):
    def __init__(self, db: DB, uid: UUID, name: str, start: Address, end: Address):
        self.db = db
        self.uid = uid
        self.name = name
        self.start = start
        self.end = end
        return


def make_uuid() -> UUID:
    raise NotImplementedError()


def verify_section_boundaries(sections: List[Section], start: Address, end: Address) -> bool:
    return True


class Module(DBObject):
    def __init__(self, db: DB, uid: UUID, name: str):
        self.db = db
        self.uid = uid
        self.name = name
        self.section_ids = []
        return

    @staticmethod
    def populate_from(db: DB, ref: DBObjectRef):
        fields = db.get(ref.uid)
        return Module(db, ref.uid, fields["name"])

    def add_section(self, name: str, start: Address, end: Address):
        sections = [Section.populate_from(self.db, DBObjectRef(uid)) for uid in self.section_ids]
        if not verify_section_boundaries(sections, start, end):
            return None
        section = Section(self.db, make_uuid(), name, start, end)
        self.section_ids.append(section.uid)
        return section


class CFG:
    def __init__(self):
        return


class SSA:
    def __init__(self):
        return


@dataclass
class IR:
    cfg: UUID
    ssa: UUID
